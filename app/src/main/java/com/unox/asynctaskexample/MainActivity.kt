package com.unox.asynctaskexample

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.image_container.view.*
import java.io.InputStream
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.URL

var fruitBitmaps = mutableListOf<Bitmap>()
val imagesUrls = listOf(
    "https://banner2.kisspng.com/20171202/e3e/fruit-free-png-image-5a22fd776d6943.7240975215122425514482.jpg",
    "http://www.pngmart.com/files/5/Red-Apple-PNG-Photos.png",
    "https://www.clipartmax.com/png/middle/66-661522_rip-banana-fruit-transparent-background-png-image-banana-png.png",
    "https://banner2.kisspng.com/20171127/d9f/fruit-basket-png-vector-clipart-image-5a1c01ee7dd394.8919843915117849425154.jpg"
)

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = MyAdapter(fruitBitmaps)
        val layoutManager = GridLayoutManager(this, 3)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager


        button_singletask_load.setOnClickListener {
            for (i in imagesUrls) {
                DownloadImageAsyncTask(adapter).execute(i)
                //DownloadImageAsyncTask(adapter).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, i)
            }
        }


        button_singletask_clear.setOnClickListener {
            fruitBitmaps.clear()
            adapter.notifyDataSetChanged()
        }
    }
}



class MyAdapter(private val dataset: List<Bitmap>) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val imageView = view.imageView
    }

    override fun getItemCount(): Int {
        return dataset.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.imageView.setImageBitmap(dataset[position])
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.image_container, parent, false)
        return ViewHolder(view)
    }
}



class DownloadImageAsyncTask(_adapter: MyAdapter) : AsyncTask<String, Unit, Bitmap>() {
    private val adapter = WeakReference(_adapter)
    private lateinit var inputStream: InputStream

    override fun doInBackground(vararg params: String?): Bitmap {
        val conn = URL(params[0]!!).openConnection() as HttpURLConnection
        conn.requestMethod = "GET"
        conn.doInput = true
        conn.connect()

        inputStream = conn.inputStream

        val bitmap = BitmapFactory.decodeStream(inputStream)
        conn.disconnect()
        inputStream.close()

        // Simulate network delay
        Thread.sleep(500)
        return bitmap
    }

    override fun onPostExecute(result: Bitmap?) {
        result?.apply {
            fruitBitmaps.add(this)
            adapter.get()?.notifyItemInserted(fruitBitmaps.size - 1)
        }
    }
}